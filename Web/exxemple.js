
let user = {
    /*nom, age et mail sont des propriétés de l'objet utilisateur
     *La valeur de la propriété "nom" est un tableau*/
    nom: 'Ben',
    age: 23,
    mail: 'explorer@digitalvalley.bj',
    //hello est une méthode de l'objet utilisateur

    hello: function () {

        alert('Hello, je suis ' + this.nom[0] + ', j\'ai ' + this.age + ' ans');
    }
};
console.log(user)

