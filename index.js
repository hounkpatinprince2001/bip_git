// Creation des variables pour la selection des 
// differents boutons et notification de la page puzzle.html

// Selectionne le bouton en fonction de son id
let puzzle_1 = document.getElementById("1");
// Selectionne le message en fonction de son message 
let puzzle_1_notif = document.getElementById('message_1');
let puzzle_2 = document.getElementById("2");
let puzzle_2_notif = document.getElementById('message_2');
let puzzle_3 = document.getElementById("3");
let puzzle_3_notif = document.getElementById('message_3');
let puzzle_4 = document.getElementById("4");
let puzzle_4_notif = document.getElementById('message_4');
let puzzle_5 = document.getElementById("5");
let puzzle_5_notif = document.getElementById('message_5');
let puzzle_6 = document.getElementById("6");
let puzzle_6_notif = document.getElementById('message_6');
let puzzle_7 = document.getElementById("7");
let puzzle_7_notif = document.getElementById('message_7');
let puzzle_8 = document.getElementById("8");
let puzzle_8_notif = document.getElementById('message_8');

// On fait tout d'abord disparaitre les messages de notification

puzzle_1_notif.style.display = 'none';
puzzle_2_notif.style.display = 'none';
puzzle_3_notif.style.display = 'none';
puzzle_4_notif.style.display = 'none';
puzzle_5_notif.style.display = 'none';
puzzle_6_notif.style.display = 'none';
puzzle_7_notif.style.display = 'none';
puzzle_8_notif.style.display = 'none';


// Creation des evenements pour l'affichage au clic des notification(pop-up)
puzzle_1.addEventListener('click', function () {
    puzzle_1_notif.style.display = 'block';
    // La notification s'affiche pendant 3 secondes
    setTimeout(function () {
        puzzle_1_notif.style.display = 'none';
    }, 3000);
});
puzzle_2.addEventListener('click', function () {
    puzzle_2_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_2_notif.style.display = 'none';
    }, 3000);
});
puzzle_3.addEventListener('click', function () {
    puzzle_3_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_3_notif.style.display = 'none';
    }, 3000);
});
puzzle_4.addEventListener('click', function () {
    puzzle_4_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_4_notif.style.display = 'none';
    }, 3000);
});
puzzle_5.addEventListener('click', function () {
    puzzle_5_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_5_notif.style.display = 'none';
    }, 3000);
});
puzzle_6.addEventListener('click', function () {
    puzzle_6_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_6_notif.style.display = 'none';
    }, 3000);
});
puzzle_7.addEventListener('click', function () {
    puzzle_7_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_7_notif.style.display = 'none';
    }, 3000);
});
puzzle_8.addEventListener('click', function () {
    puzzle_8_notif.style.display = 'block';
    setTimeout(function () {
        puzzle_8_notif.style.display = 'none';
    }, 3000);
});
